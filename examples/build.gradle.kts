plugins {
    kotlin("jvm")
    id("java-test-fixtures")
}

repositories {
    mavenCentral()
}
