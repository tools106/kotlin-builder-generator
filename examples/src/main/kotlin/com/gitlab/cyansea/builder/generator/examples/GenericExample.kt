@file:Suppress("unused")

package com.gitlab.cyansea.builder.generator.examples

import java.time.LocalDate
import java.time.OffsetDateTime

data class GenericExample(
    val i: Int,
    val l: Long,
    val b: Boolean,
    val s: String,
    val opt: Int?,
    val list: List<Int>,
    val nullableList: List<Boolean>?,
    val set: Set<Int>,
    val custom: Node?,
    val coll: Collection<Int>,
    val date: LocalDate,
    val modified: OffsetDateTime,
    val matrix: List<List<Int>>,
    val phoneBook: Map<String, String>,
    val nodesById: Map<String, Node>
)

// Interfaces, enums and sealed or abstract classes ignored below
enum class Temperatures { HOT, COLD }
abstract class BaseNode
sealed class Operator(val left: Node, val right: Node) : BaseNode()
class Minus(left: Node, right: Node) : Operator(left, right)
class Plus(left: Node, right: Node) : Operator(left, right)

// If class has multiple constructors, builder only considers the primary one
class Node(
    val value: Int,
    val id: String
) {
    constructor(valueAndId: String) : this(valueAndId.substringBefore(':').toInt(), valueAndId.substringAfter(':'))
}
