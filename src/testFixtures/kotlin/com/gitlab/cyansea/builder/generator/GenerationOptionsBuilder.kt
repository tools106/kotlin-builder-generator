package com.gitlab.cyansea.builder.generator

import com.gitlab.cyansea.builder.generator.lib.utils.io.FileOutput
import com.gitlab.cyansea.builder.generator.lib.utils.io.Output
import com.gitlab.cyansea.builder.generator.lib.utils.io.StringOutput
import kotlinx.ast.common.AstSource

class GenerationOptionsBuilder {
    private var source: AstSource = AstSource.String("", content = "")
    private var output: Output = StringOutput()
    private var lineEnding: LineEnding = LineEnding.LF

    fun source(source: AstSource) = apply { this.source = source }

    fun output(output: Output) = apply { this.output = output }

    fun lineEnding(lineEnding: LineEnding) = apply { this.lineEnding = lineEnding }

    fun cliDefaults() =
        output(FileOutput("output.kt"))
            .lineEnding(LineEnding.systemDefault())

    fun build(): GenerationOptions = GenerationOptions(
        source = source,
        output = output,
        lineEnding = lineEnding,
    )

    companion object {
        fun build(): GenerationOptions = GenerationOptionsBuilder().build()

        fun build(builderSequence: GenerationOptionsBuilder.() -> GenerationOptionsBuilder): GenerationOptions =
            GenerationOptionsBuilder().builderSequence().build()
    }
}

