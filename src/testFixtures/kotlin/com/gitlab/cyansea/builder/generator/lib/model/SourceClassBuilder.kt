package com.gitlab.cyansea.builder.generator.lib.model

class SourceClassBuilder {
    private var name: String = ""
    private var properties: MutableList<Property> = mutableListOf()

    fun name(name: String) = apply { this.name = name }
    fun properties(properties: MutableList<Property>) = apply { this.properties = properties }
    fun property(builderSequence: PropertyBuilder.() -> PropertyBuilder) = apply {
        this.properties.add(PropertyBuilder.build(builderSequence))
    }

    fun intProperty(name: String) = property {
        name(name)
        rawType("Int")
        type { identifier("Int") }
    }

    fun valid() = name("TestClass")

    fun build(): SourceClass = SourceClass(
        name = name,
        properties = properties,
    )

    companion object {
        fun build(): SourceClass = SourceClassBuilder().build()

        fun build(builderSequence: SourceClassBuilder.() -> SourceClassBuilder): SourceClass =
            SourceClassBuilder().builderSequence().build()
    }
}
