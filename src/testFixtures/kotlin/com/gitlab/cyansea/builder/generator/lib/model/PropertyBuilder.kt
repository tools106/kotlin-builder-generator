package com.gitlab.cyansea.builder.generator.lib.model

class PropertyBuilder {
    private var name: String = ""
    private var rawType: String = ""
    private var type: Type = TypeBuilder.build()

    fun name(name: String) = apply { this.name = name }
    fun rawType(rawType: String) = apply { this.rawType = rawType }
    fun type(type: Type) = apply { this.type = type }
    fun type(builderSequence: TypeBuilder.() -> TypeBuilder) = type(TypeBuilder.build(builderSequence))

    fun valid() = name("prop")
        .rawType("rawType")
        .type { valid() }

    fun build(): Property = Property(
        name = name,
        rawType = rawType,
        type = type,
    )

    companion object {
        fun build(): Property = PropertyBuilder().build()

        fun build(builderSequence: PropertyBuilder.() -> PropertyBuilder): Property =
            PropertyBuilder().builderSequence().build()
    }
}
