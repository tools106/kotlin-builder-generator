package com.gitlab.cyansea.builder.generator.lib.model

class TypeBuilder {
    private var identifier: String = ""
    private var isNullable: Boolean = false
    private var parameters: MutableList<Type> = mutableListOf()

    fun identifier(identifier: String) = apply { this.identifier = identifier }

    fun isNullable(isNullable: Boolean) = apply { this.isNullable = isNullable }

    fun parameters(parameters: MutableList<Type>) = apply { this.parameters = parameters }
    fun addParameters(parameters: Type) = apply { this.parameters.add(parameters) }
    fun addParameters(builderSequence: TypeBuilder.() -> TypeBuilder) = addParameters(build(builderSequence))

    fun valid() = identifier("MyType")

    fun build(): Type = Type(
        identifier = identifier,
        isNullable = isNullable,
        parameters = parameters,
    )

    companion object {
        fun build(): Type = TypeBuilder().build()

        fun build(builderSequence: TypeBuilder.() -> TypeBuilder): Type = TypeBuilder().builderSequence().build()
    }
}
