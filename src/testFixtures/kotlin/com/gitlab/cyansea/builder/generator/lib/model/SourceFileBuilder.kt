package com.gitlab.cyansea.builder.generator.lib.model

class SourceFileBuilder {
    private var classes: MutableList<SourceClass> = mutableListOf()
    private var imports: MutableList<String> = mutableListOf()
    private var packageDeclaration: String? = null

    fun classes(classes: MutableList<SourceClass>) = apply { this.classes = classes }
    fun classes(builderSequence: SourceClassBuilder.() -> SourceClassBuilder) = apply {
        this.classes.add(SourceClassBuilder.build(builderSequence))
    }

    fun imports(imports: MutableList<String>) = apply { this.imports = imports }
    fun packageDeclaration(packageDeclaration: String?) = apply { this.packageDeclaration = packageDeclaration }

    fun build(): SourceFile = SourceFile(
        classes = classes,
        imports = imports,
        packageDeclaration = packageDeclaration,
    )

    companion object {
        fun build(): SourceFile = SourceFileBuilder().build()

        fun build(builderSequence: SourceFileBuilder.() -> SourceFileBuilder): SourceFile =
            SourceFileBuilder().builderSequence().build()
    }
}
