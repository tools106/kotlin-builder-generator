package com.gitlab.cyansea.builder.generator.lib.internal

import com.gitlab.cyansea.builder.generator.lib.model.Property
import com.gitlab.cyansea.builder.generator.lib.model.TypeBuilder
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SourceFileAnalyzerImplTest {
    private val sourceFileAnalyzerImpl = SourceFileAnalyzerImpl()

    @Test
    fun analyze_whenInputHasNoPackageHeader_packageDeclarationNull() {
        val input = buildAst("")

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.packageDeclaration).isNull()
    }

    @Test
    fun analyze_whenInputHasSimplePackage_packageDeclarationCorrectlyAnalyzed() {
        val input = buildAst("package simple")

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.packageDeclaration).isEqualTo("simple")
    }

    @Test
    fun analyze_whenInputHasMultiPartPackage_packageDeclarationCorrectlyAnalyzed() {
        val input = buildAst("package com.gitlab.cyansea.builder.generator.lib.internal")

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.packageDeclaration).isEqualTo("com.gitlab.cyansea.builder.generator.lib.internal")
    }

    @Test
    fun analyze_whenInputHasDataClass_correctlyPicksUpClassName() {
        val input = buildAst(
            """
            data class Test(val i: Int)
            """.trimIndent()
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        assertThat(analyzedSource.classes.first().name).isEqualTo("Test")
    }

    @Test
    fun analyze_whenInputDataClassHasBasicProperty_determinesPropertyTypeAndName() {
        val input = buildAst(
            """
            data class Test(val i: Int)
            """.trimIndent()
        )
        val expectedProperty = Property(
            name = "i",
            rawType = "Int",
            type = TypeBuilder.build { identifier("Int").isNullable(false) }
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        val sourceClass = analyzedSource.classes.first()
        assertThat(sourceClass.properties).hasSize(1)
        val property = sourceClass.properties.first()
        assertThat(property).isEqualTo(expectedProperty)
    }

    @Test
    fun analyze_whenInputDataClassHasNullableProperty_determinesPropertyTypeAndNullability() {
        val input = buildAst(
            """
            data class Test(val i: Int?)
            """.trimIndent()
        )
        val expectedProperty = Property(
            name = "i",
            rawType = "Int?",
            type = TypeBuilder.build {
                identifier("Int")
                isNullable(true)
            }
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        val sourceClass = analyzedSource.classes.first()
        assertThat(sourceClass.properties).hasSize(1)
        val property = sourceClass.properties.first()
        assertThat(property).isEqualTo(expectedProperty)
    }

    @Test
    fun analyze_whenInputDataClassHasPropertyWithTypeArguments_determinesPropertyType() {
        val input = buildAst(
            """
            data class Test(val i: List<Int>)
            """.trimIndent()
        )
        val expectedProperty = Property(
            name = "i",
            rawType = "List<Int>",
            type = TypeBuilder.build {
                identifier("List")
                addParameters {
                    identifier("Int")
                }
            }
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        val sourceClass = analyzedSource.classes.first()
        assertThat(sourceClass.properties).hasSize(1)
        val property = sourceClass.properties.first()
        assertThat(property).isEqualTo(expectedProperty)
    }

    @Test
    fun analyze_whenInputDataClassHasPropertyWithTypeArguments_analyzesTypeRecursively() {
        val input = buildAst(
            """
            data class Test(val matrix: List<Array<Int>>)
            """.trimIndent()
        )
        val expectedProperty = Property(
            name = "matrix",
            rawType = "List<Array<Int>>",
            type = TypeBuilder.build {
                identifier("List")
                addParameters {
                    identifier("Array")
                    addParameters {
                        identifier("Int")
                    }
                }
            }
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        val sourceClass = analyzedSource.classes.first()
        assertThat(sourceClass.properties).hasSize(1)
        val property = sourceClass.properties.first()
        assertThat(property).isEqualTo(expectedProperty)
    }

    @Test
    fun analyze_whenInputFileHasImport_includesItInOutput() {
        val input = buildAst(
            """
            import my.company.project.File
            import my.company.project.MyClass
            """.trimIndent()
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.imports).containsExactlyInAnyOrder(
            "my.company.project.File",
            "my.company.project.MyClass"
        )
    }

    @Test
    fun analyze_whenInputContainsEnumClass_ignoresIt() {
        val input = buildAst(
            """
                enum class Sign { MINUS, PLUS }
            """.trimIndent()
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).isEmpty()
    }

    @Test
    fun analyze_whenInputContainsInterface_ignoresIt() {
        val input = buildAst(
            """
                interface ICalculator
            """.trimIndent()
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).isEmpty()
    }

    @Test
    fun analyze_whenInputContainsSealedClass_ignoresIt() {
        val input = buildAst(
            """
                sealed class Operator
            """.trimIndent()
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).isEmpty()
    }

    @Test
    fun analyze_whenInputContainsAbstractClass_ignoresIt() {
        val input = buildAst(
            """
                abstract class BaseOperator
            """.trimIndent()
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).isEmpty()
    }

    @Test
    fun analyze_whenInputIsNonDataClassWithOnlySingleConstructor_analysisAsForDataClasses() {
        val input = buildAst(
            """
            class Test(val i: List<Int>)
            """.trimIndent()
        )
        val expectedProperty = Property(
            name = "i",
            rawType = "List<Int>",
            type = TypeBuilder.build {
                identifier("List")
                addParameters {
                    identifier("Int")
                }
            }
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        val sourceClass = analyzedSource.classes.first()
        assertThat(sourceClass.properties).hasSize(1)
        val property = sourceClass.properties.first()
        assertThat(property).isEqualTo(expectedProperty)
    }

    @Test
    fun analyze_whenInputClassHasPropertiesNotUsedByConstructor_theyAreIgnored() {
        val input = buildAst(
            """
            class Test(val minutes: Long) {
                val seconds = minutes * 60
            }
            """.trimIndent()
        )
        val expectedProperty = Property(
            name = "minutes",
            rawType = "Long",
            type = TypeBuilder.build { identifier("Long") }
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        val sourceClass = analyzedSource.classes.first()
        assertThat(sourceClass.properties).hasSize(1)
        val property = sourceClass.properties.first()
        assertThat(property).isEqualTo(expectedProperty)
    }

    @Test
    fun analyze_whenInputClassHasMultipleConstructors_onlyPropertiesOfPrimaryConstructorAreAnalyzed() {
        val input = buildAst(
            """
            class Test(val minutes: Long) {
                constructor(hours: Int) : this(hours.toLong() * 60)
            }
            """.trimIndent()
        )
        val expectedProperty = Property(
            name = "minutes",
            rawType = "Long",
            type = TypeBuilder.build { identifier("Long") }
        )

        val analyzedSource = sourceFileAnalyzerImpl.analyze(input)

        assertThat(analyzedSource.classes).hasSize(1)
        val sourceClass = analyzedSource.classes.first()
        assertThat(sourceClass.properties).hasSize(1)
        val property = sourceClass.properties.first()
        assertThat(property).isEqualTo(expectedProperty)
    }
}
