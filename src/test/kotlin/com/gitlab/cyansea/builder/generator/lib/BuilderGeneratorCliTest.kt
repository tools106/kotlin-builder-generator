package com.gitlab.cyansea.builder.generator.lib

import com.gitlab.cyansea.builder.generator.BuilderGeneratorCli
import com.gitlab.cyansea.builder.generator.GenerationOptionsBuilder
import com.gitlab.cyansea.builder.generator.LineEnding
import com.gitlab.cyansea.builder.generator.lib.utils.io.FileOutput
import com.gitlab.cyansea.builder.generator.lib.utils.io.SystemExitManager
import kotlinx.ast.common.AstSource
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.kotlin.any
import org.mockito.kotlin.given
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify

class BuilderGeneratorCliTest {
    private val builderGenerationController = mock<BuilderGenerationController>()
    private val systemExitManager = mock<SystemExitManager>()
    private val builderGeneratorCli = BuilderGeneratorCli(
        builderGenerationController = builderGenerationController,
        systemExitManager = systemExitManager
    )

    @BeforeEach
    fun mockGenerationController() {
        given(builderGenerationController.generate(any()))
            .will { }
    }

    @ParameterizedTest
    @ValueSource(strings = ["-s", "--source"])
    fun setsConfiguredSourceFileAsASTSource(sourceOptionName: String) {
        val expectedConfiguration = GenerationOptionsBuilder.build {
            cliDefaults()
            source(AstSource.File("input.kt"))
        }

        builderGeneratorCli.run(arrayOf(sourceOptionName, "input.kt"))

        verify(builderGenerationController, times(1)).generate(expectedConfiguration)
    }

    @ParameterizedTest
    @ValueSource(strings = ["-o", "--output"])
    fun setsConfiguredOutputFileIntoConfiguration(outputOptionName: String) {
        val expectedConfiguration = GenerationOptionsBuilder.build {
            cliDefaults()
            source(AstSource.File("input.kt"))
            output(FileOutput("different.kt"))
        }

        builderGeneratorCli.run(arrayOf("-s", "input.kt", outputOptionName, "different.kt"))

        verify(builderGenerationController, times(1)).generate(expectedConfiguration)
    }

    @ParameterizedTest
    @ValueSource(strings = ["-l", "--line-ending"])
    fun supportsCRLFLineEndings(lineEndingOptionName: String) {
        val expectedConfiguration = GenerationOptionsBuilder.build {
            cliDefaults()
            source(AstSource.File("input.kt"))
            lineEnding(LineEnding.CRLF)
        }

        builderGeneratorCli.run(arrayOf("-s", "input.kt", lineEndingOptionName, "crlf"))

        verify(builderGenerationController, times(1)).generate(expectedConfiguration)
    }

    @ParameterizedTest
    @ValueSource(strings = ["-l", "--line-ending"])
    fun supportsLFLineEndings(lineEndingOptionName: String) {
        val expectedConfiguration = GenerationOptionsBuilder.build {
            cliDefaults()
            source(AstSource.File("input.kt"))
            lineEnding(LineEnding.LF)
        }

        builderGeneratorCli.run(arrayOf("-s", "input.kt", lineEndingOptionName, "lf"))

        verify(builderGenerationController, times(1)).generate(expectedConfiguration)
    }

    @ParameterizedTest
    @ValueSource(strings = ["-g", "--generate-to-test-fixtures"])
    fun setsOutputToTestFixturesOf(testFixturesFlagName: String) {
        val expectedConfiguration = GenerationOptionsBuilder.build {
            cliDefaults()
            source(AstSource.File("src/main/com/project/Hello.kt"))
            output(FileOutput("src/testFixtures/com/project/HelloBuilder.kt"))
        }

        builderGeneratorCli.run(arrayOf("-s", "src/main/com/project/Hello.kt", testFixturesFlagName))

        verify(builderGenerationController, times(1)).generate(expectedConfiguration)
    }

    @Test
    fun supportGenerationToTestFixturesWhenSourcePathDoesNotBeginAtRoot() {
        val expectedConfiguration = GenerationOptionsBuilder.build {
            cliDefaults()
            source(AstSource.File("my/home/project/app/src/main/com/project/Hello.kt"))
            output(FileOutput("my/home/project/app/src/testFixtures/com/project/HelloBuilder.kt"))
        }

        builderGeneratorCli.run(arrayOf("-s", "my/home/project/app/src/main/com/project/Hello.kt", "-g"))

        verify(builderGenerationController, times(1)).generate(expectedConfiguration)
    }

    @Test
    fun testFixturesGeneration_IfNoSrcDirectoryInSourcePath_ExitsProcessWithStatus1() {
        given(systemExitManager.exit(1))
            .willThrow(RuntimeException(""))

        assertThatThrownBy {
            builderGeneratorCli.run(arrayOf("-s", "main/com/project/Hello.kt", "-g"))
        }
            .isExactlyInstanceOf(RuntimeException::class.java)
            .hasMessage("")
    }

    @Test
    fun testFixturesGeneration_IfMultipleSrcDirectoriesInSourcePath_ExitsProcessWithStatus1() {
        given(systemExitManager.exit(1))
            .willThrow(RuntimeException(""))

        assertThatThrownBy {
            builderGeneratorCli.run(arrayOf("-s", "root/src/project/src/main/com/project/Hello.kt", "-g"))
        }
            .isExactlyInstanceOf(RuntimeException::class.java)
            .hasMessage("")
    }
}
