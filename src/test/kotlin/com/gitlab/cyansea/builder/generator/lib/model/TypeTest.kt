package com.gitlab.cyansea.builder.generator.lib.model

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test

class TypeTest {
    @Test
    fun validate_isValid_throwsNothing() {
        val type = TypeBuilder.build { valid() }

        assertThatCode { type.validate() }.doesNotThrowAnyException()
    }

    @Test
    fun validate_whenIdentifierBlank_throwsIllegalArgumentException() {
        val type = TypeBuilder.build { valid().identifier("   \n\t  ") }

        val thrown = catchThrowable { type.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Type identifier cannot be blank.")
    }

    @Test
    fun validate_whenParameterInvalid_throwsIllegalArgumentException() {
        val type = TypeBuilder.build {
            valid()
            addParameters(
                TypeBuilder.build()
            )
        }

        val thrown = catchThrowable { type.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Type identifier cannot be blank.")
    }
}
