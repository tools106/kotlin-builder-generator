package com.gitlab.cyansea.builder.generator.lib.model

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test

class SourceClassTest {

    @Test
    fun validate_whenIsValid_throwsNothing() {
        val sourceClass = SourceClassBuilder.build { valid() }

        assertThatCode { sourceClass.validate() }.doesNotThrowAnyException()
    }

    @Test
    fun validate_whenNameBlank_throwsIllegalArgumentException() {
        val sourceClass = SourceClassBuilder.build { valid().name("   \n\t  ") }

        val thrown = catchThrowable { sourceClass.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Class name cannot be blank")
    }

    @Test
    fun validate_whenPropertyInvalid_throwsIllegalArgumentException() {
        val sourceClass = SourceClassBuilder.build { valid().intProperty("        ") }

        val thrown = catchThrowable { sourceClass.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Property name cannot be blank.")
    }
}
