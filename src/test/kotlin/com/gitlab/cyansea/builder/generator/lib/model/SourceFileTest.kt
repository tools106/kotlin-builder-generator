package com.gitlab.cyansea.builder.generator.lib.model

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test

class SourceFileTest {
    @Test
    fun validate_whenIsValid_throwsNothing() {
        val sourceFile = SourceFileBuilder.build()

        assertThatCode { sourceFile.validate() }.doesNotThrowAnyException()
    }

    @Test
    fun validate_whenHasInvalidClass_throwsIllegalArgumentException() {
        val sourceFile = SourceFileBuilder.build {
            classes {
                name("      \n\t")
            }
        }

        val thrown = catchThrowable { sourceFile.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Class name cannot be blank")
    }
}
