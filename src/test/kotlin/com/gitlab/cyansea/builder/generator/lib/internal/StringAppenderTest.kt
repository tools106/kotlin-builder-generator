package com.gitlab.cyansea.builder.generator.lib.internal

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class StringAppenderTest {
    @Test
    fun toString_returnsEmptyString_whenNoOperationsDone() {
        val stringAppender = StringAppender("")

        assertThat(stringAppender.toString()).isEqualTo("")
    }

    @Test
    fun append_addsGivenTextToToStringOutput() {
        val stringAppender = StringAppender("")

        stringAppender.append("Hello")

        assertThat(stringAppender.toString()).isEqualTo("Hello")
    }

    @Test
    fun appendLine_addsConfiguredLineSeparatorToOutput() {
        val stringAppender = StringAppender("\n")

        stringAppender.appendLine()

        assertThat(stringAppender.toString()).isEqualTo("\n")
    }

    @Test
    fun appendLine_addsGivenTextAndLineSeparatorToOutput() {
        val stringAppender = StringAppender("\n")

        stringAppender.appendLine("Hello!")

        assertThat(stringAppender.toString()).isEqualTo("Hello!\n")
    }

    @Test
    fun appendLine_canBeChainedToAddMultipleLines() {
        val stringAppender = StringAppender("\n")

        stringAppender.appendLine("Hello!")
            .appendLine()
            .appendLine("Been a while hasn't it?")
            .append("Best regards, ")
            .appendLine("Yours truly")

        val expected = """
            Hello!
            
            Been a while hasn't it?
            Best regards, Yours truly

        """.trimIndent()
        assertThat(stringAppender.toString()).isEqualTo(expected)
    }

    @Test
    fun appendLine_addsConfiguredIndentToBeginningOfLine() {
        val stringAppender = StringAppender(lineSeparator = "\n").increaseIndent()
        val expected = """    Hello
            |    World!
            |
        """.trimMargin()

        stringAppender
            .appendLine("Hello")
            .appendLine("World!")

        assertThat(stringAppender.toString()).isEqualTo(expected)
    }

    @Test
    fun increaseIndent_increasesIndentByIndentStep() {
        val stringAppender = StringAppender(lineSeparator = "\n", indentStep = 2).increaseIndent()

        val expected = """  Hello
            |  World!
            |
        """.trimMargin()

        stringAppender
            .appendLine("Hello")
            .appendLine("World!")

        assertThat(stringAppender.toString()).isEqualTo(expected)
    }

    @Test
    fun decreaseIndent_decreasesIndentByIndentStep() {
        val stringAppender = StringAppender(lineSeparator = "\n").increaseIndent()
            .decreaseIndent()
        val expected = """Hello
            |World!
            |
        """.trimMargin()

        stringAppender
            .appendLine("Hello")
            .appendLine("World!")

        assertThat(stringAppender.toString()).isEqualTo(expected)
    }
}
