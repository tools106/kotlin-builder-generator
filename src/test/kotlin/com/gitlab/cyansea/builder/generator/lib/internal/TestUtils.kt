package com.gitlab.cyansea.builder.generator.lib.internal

import kotlinx.ast.common.AstSource
import kotlinx.ast.common.ast.Ast
import kotlinx.ast.grammar.kotlin.target.antlr.kotlin.KotlinGrammarAntlrKotlinParser

/**
 * Build AST from [fileContents]
 */
fun buildAst(fileContents: String): Ast {
    val source = AstSource.String("testinput", content = fileContents)
    return KotlinGrammarAntlrKotlinParser.parseKotlinFile(source)
}
