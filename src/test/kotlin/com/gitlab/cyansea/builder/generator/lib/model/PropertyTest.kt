package com.gitlab.cyansea.builder.generator.lib.model

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.assertj.core.api.Assertions.catchThrowable
import org.junit.jupiter.api.Test

class PropertyTest {
    @Test
    fun validate_isValid_throwsNothing() {
        val property = PropertyBuilder.build { valid() }

        assertThatCode { property.validate() }.doesNotThrowAnyException()
    }

    @Test
    fun validate_whenNameBlank_throwsIllegalArgumentException() {
        val property = PropertyBuilder.build { valid().name("   \n\t  ") }

        val thrown = catchThrowable { property.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Property name cannot be blank.")
    }

    @Test
    fun validate_whenRawTypeBlank_throwsIllegalArgumentException() {
        val property = PropertyBuilder.build { valid().rawType("   \n\t\n  ") }

        val thrown = catchThrowable { property.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Property raw type cannot be blank.")
    }

    @Test
    fun validate_whenTypeInvalid_throwsIllegalArgumentException() {
        val property = PropertyBuilder.build {
            valid()
            type(TypeBuilder.build { identifier("    \n\t ") })
        }

        val thrown = catchThrowable { property.validate() }

        assertThat(thrown).isExactlyInstanceOf(IllegalArgumentException::class.java)
        assertThat(thrown).hasMessage("Type identifier cannot be blank.")
    }
}
