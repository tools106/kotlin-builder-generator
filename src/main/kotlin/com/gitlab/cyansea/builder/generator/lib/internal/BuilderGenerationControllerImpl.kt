package com.gitlab.cyansea.builder.generator.lib.internal

import com.gitlab.cyansea.builder.generator.GenerationOptions
import com.gitlab.cyansea.builder.generator.lib.BuilderGenerationController
import kotlinx.ast.grammar.kotlin.target.antlr.kotlin.KotlinGrammarAntlrKotlinParser

class BuilderGenerationControllerImpl : BuilderGenerationController {
    override fun generate(generationOptions: GenerationOptions) {
        val ast = KotlinGrammarAntlrKotlinParser.parseKotlinFile(generationOptions.source)

        val sourceCodeAnalyzer = SourceFileAnalyzerImpl()
        val builderGenerator = BuilderGeneratorImpl(generationOptions.toGeneratorConfiguration())

        val analyzedSource = sourceCodeAnalyzer.analyze(ast)
        val builderSourceCode = builderGenerator.generate(analyzedSource)
        
        generationOptions.output.writeText(builderSourceCode)
    }
}
