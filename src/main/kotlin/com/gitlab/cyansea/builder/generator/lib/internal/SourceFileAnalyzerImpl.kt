package com.gitlab.cyansea.builder.generator.lib.internal

import com.gitlab.cyansea.builder.generator.lib.SourceFileAnalyzer
import com.gitlab.cyansea.builder.generator.lib.model.Property
import com.gitlab.cyansea.builder.generator.lib.model.SourceClass
import com.gitlab.cyansea.builder.generator.lib.model.SourceFile
import com.gitlab.cyansea.builder.generator.lib.model.Type
import kotlinx.ast.common.ast.Ast
import kotlinx.ast.common.ast.DefaultAstNode
import kotlinx.ast.common.klass.KlassDeclaration
import kotlinx.ast.common.klass.KlassIdentifier
import kotlinx.ast.grammar.kotlin.common.summary
import kotlinx.ast.grammar.kotlin.common.summary.Import
import kotlinx.ast.grammar.kotlin.common.summary.PackageHeader

class SourceFileAnalyzerImpl : SourceFileAnalyzer {
    override fun analyze(ast: Ast): SourceFile {
        val summary = ast.summary(attachRawAst = false).get()

        val packageDeclaration = analyzePackageHeader(summary)
        val imports = analyzeImports(summary)
        val classDeclarations = summary.filterIsInstance<KlassDeclaration>()
            .filterNotAnyOf(::isAbstract, ::isEnum, ::isInterface, ::isSealed)
            .map(::analyzeClass)

        return SourceFile(
            classes = classDeclarations,
            imports = imports,
            packageDeclaration = packageDeclaration,
        )
    }

    private fun <T> Iterable<T>.filterNotAnyOf(vararg predicates: (T) -> Boolean): Iterable<T> = filterNot { element ->
        predicates.any { it.invoke(element) }
    }

    private fun isInterface(klassDeclaration: KlassDeclaration) = klassDeclaration.keyword == "interface"
    private fun isAbstract(klassDeclaration: KlassDeclaration) =
        klassDeclaration.modifiers.any { it.modifier == "abstract" }

    private fun isEnum(klassDeclaration: KlassDeclaration) = klassDeclaration.modifiers.any { it.modifier == "enum" }
    private fun isSealed(klassDeclaration: KlassDeclaration) =
        klassDeclaration.modifiers.any { it.modifier == "sealed" }

    private fun analyzeImports(summary: List<Ast>): List<String> {
        val importList = summary.first { it.description == "importList" } as DefaultAstNode

        return importList
            .children
            .filterIsInstance<Import>()
            .map { import ->
                import.identifier.joinToString(separator = ".") { it.identifier }
            }
    }

    private fun analyzeClass(klassDeclaration: KlassDeclaration): SourceClass {
        val id = klassDeclaration.identifier!!.identifier

        val primaryConstructor = klassDeclaration.parameter.first(::isConstructor)
        val properties = primaryConstructor.parameter.map { parameter ->
            if (parameter.type.size != 1) {
                throw IllegalArgumentException("Builder cannot be generated for class $id parameter type ${parameter.identifier?.identifier}")
            }
            val type = parameter.type.first()
            Property(
                name = parameter.identifier!!.identifier,
                rawType = type.rawName,
                type = analyzeType(type)
            )
        }

        return SourceClass(
            name = id,
            properties = properties
        )
    }

    private fun isConstructor(klassDeclaration: KlassDeclaration) = klassDeclaration.keyword == "constructor"

    private fun analyzeType(klassIdentifier: KlassIdentifier): Type = with(klassIdentifier) {
        Type(
            identifier = identifier,
            isNullable = nullable,
            parameters = parameter.map(::analyzeType)
        )
    }

    private fun analyzePackageHeader(summary: List<Ast>): String? {
        val packageDeclarations = summary.filterIsInstance(PackageHeader::class.java)
        if (packageDeclarations.size != 1) {
            throw IllegalArgumentException("More than one package declaration found!")
        }
        return determinePackageDeclaration(packageDeclarations.first())
    }

    private fun determinePackageDeclaration(packageHeader: PackageHeader): String? {
        if (packageHeader.identifier.isEmpty()) {
            // No package declaration in source
            return null
        }

        return packageHeader
            .identifier
            .joinToString(separator = ".", transform = KlassIdentifier::identifier)
    }
}
