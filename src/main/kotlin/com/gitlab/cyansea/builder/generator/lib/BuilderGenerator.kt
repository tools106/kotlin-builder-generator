package com.gitlab.cyansea.builder.generator.lib

import com.gitlab.cyansea.builder.generator.lib.model.SourceFile

interface BuilderGenerator {
    fun generate(sourceFile: SourceFile): String
}
