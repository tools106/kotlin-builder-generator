package com.gitlab.cyansea.builder.generator.lib.internal

class StringAppender(
    private val lineSeparator: String,
    private val indentStep: Int = 4,
) {
    private val currentIndent = StringBuilder()
    private val stringBuilder: StringBuilder = StringBuilder()

    fun increaseIndent(): StringAppender = apply {
        repeat(indentStep) {
            currentIndent.append(" ")
        }
    }

    fun decreaseIndent(): StringAppender = apply {
        require(currentIndent.length >= indentStep) { "Cannot decrease indent as it is less than configured step" }
        currentIndent.delete(currentIndent.length - indentStep, currentIndent.length)
    }

    fun append(value: String): StringAppender = apply {
        stringBuilder.append(value)
    }

    fun appendLine(): StringAppender = apply {
        stringBuilder.append(lineSeparator)
    }

    /**
     * Appends [value] indented by [currentIndent] on the current line.
     */
    fun appendLine(value: String) = apply {
        append(currentIndent.toString())
        append(value)
        appendLine()
    }

    override fun toString(): String = stringBuilder.toString()
}
