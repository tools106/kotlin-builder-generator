package com.gitlab.cyansea.builder.generator.lib

import com.gitlab.cyansea.builder.generator.GenerationOptions

interface BuilderGenerationController {
    fun generate(generationOptions: GenerationOptions)
}
