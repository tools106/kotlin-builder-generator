package com.gitlab.cyansea.builder.generator

import com.gitlab.cyansea.builder.generator.lib.Configuration
import com.gitlab.cyansea.builder.generator.lib.utils.io.Output
import kotlinx.ast.common.AstSource

data class GenerationOptions(
    val source: AstSource,
    val output: Output,
    val lineEnding: LineEnding
) {
    fun toGeneratorConfiguration(): Configuration = Configuration(
        lineSeparator = lineEnding.toLineBreak()
    )
}
