package com.gitlab.cyansea.builder.generator.lib.utils.io

class StringOutput : Output {
    private val output = StringBuilder()

    override fun writeText(text: String) {
        output.append(text)
    }

    fun getContents(): String = output.toString()
}
