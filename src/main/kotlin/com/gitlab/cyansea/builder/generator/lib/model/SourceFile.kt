package com.gitlab.cyansea.builder.generator.lib.model

data class SourceFile(
    val classes: Collection<SourceClass>,
    val packageDeclaration: String?,
    val imports: Collection<String>
) {
    fun validate() {
        classes.forEach(SourceClass::validate)
    }
}
