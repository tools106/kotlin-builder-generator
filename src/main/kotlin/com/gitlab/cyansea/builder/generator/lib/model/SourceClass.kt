package com.gitlab.cyansea.builder.generator.lib.model

data class SourceClass(
    val name: String,
    val properties: List<Property>
) {
    fun validate() {
        require(name.isNotBlank()) { "Class name cannot be blank" }

        properties.forEach(Property::validate)
    }
}
