package com.gitlab.cyansea.builder.generator

enum class LineEnding {
    CRLF {
        override fun toLineBreak(): String = "\r\n"
    },
    LF {
        override fun toLineBreak(): String = "\n"
    };

    abstract fun toLineBreak(): String

    companion object {
        fun systemDefault(): LineEnding {
            return values().find { it.toLineBreak() == System.lineSeparator() }
                ?: throw IllegalArgumentException("Unrecognized line ending style")
        }
    }
}
