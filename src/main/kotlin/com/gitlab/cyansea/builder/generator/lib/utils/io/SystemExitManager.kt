package com.gitlab.cyansea.builder.generator.lib.utils.io

interface SystemExitManager {
    fun exit(status: Int)
}
