package com.gitlab.cyansea.builder.generator.lib.model

data class Type(
    val identifier: String,
    val isNullable: Boolean,
    val parameters: List<Type>
) {
    fun validate() {
        require(identifier.isNotBlank()) { "Type identifier cannot be blank." }
        parameters.forEach(Type::validate)
    }
}
