package com.gitlab.cyansea.builder.generator.lib.model

data class Property(
    val name: String,
    /**
     * Raw type name from source file. Includes for instance ? if it is nullable.
     */
    val rawType: String,
    val type: Type
) {
    fun validate() {
        require(name.isNotBlank()) { "Property name cannot be blank." }
        require(rawType.isNotBlank()) { "Property raw type cannot be blank." }
        type.validate()
    }
}
