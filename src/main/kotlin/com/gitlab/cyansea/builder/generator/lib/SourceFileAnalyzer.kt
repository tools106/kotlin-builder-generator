package com.gitlab.cyansea.builder.generator.lib

import com.gitlab.cyansea.builder.generator.lib.model.SourceFile
import kotlinx.ast.common.ast.Ast

interface SourceFileAnalyzer {
    fun analyze(ast: Ast): SourceFile
}
