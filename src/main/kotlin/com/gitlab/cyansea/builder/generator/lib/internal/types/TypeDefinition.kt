package com.gitlab.cyansea.builder.generator.lib.internal.types

data class TypeDefinition(
    val identifier: String,
    val isCollection: Boolean = false,
    val isMap: Boolean = false,
    val defaultValue: String,
    val generatedTypeOverride: String? = null
)
