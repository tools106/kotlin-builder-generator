package com.gitlab.cyansea.builder.generator

import com.gitlab.cyansea.builder.generator.lib.BuilderGenerationController
import com.gitlab.cyansea.builder.generator.lib.internal.BuilderGenerationControllerImpl
import com.gitlab.cyansea.builder.generator.lib.utils.io.FileOutput
import com.gitlab.cyansea.builder.generator.lib.utils.io.SystemExitManager
import com.gitlab.cyansea.builder.generator.lib.utils.io.SystemExitManagerImpl
import kotlinx.ast.common.AstSource
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import kotlinx.cli.required
import kotlin.io.path.Path
import kotlin.io.path.name

class BuilderGeneratorCli(
    private val builderGenerationController: BuilderGenerationController,
    private val systemExitManager: SystemExitManager = SystemExitManagerImpl()
) {
    fun run(args: Array<String>) {
        val configuration = parseArgs(args)

        builderGenerationController.generate(configuration)
    }

    private fun parseArgs(args: Array<String>): GenerationOptions {
        val parser = ArgParser("BuilderGeneratorCli")

        val sourceFile by parser.option(
            ArgType.String, shortName = "s", fullName = "source", description = "Source file path"
        ).required()

        val outputFile by parser.option(
            ArgType.String, shortName = "o", fullName = "output", description = "Output file path"
        ).default("output.kt")

        val generateToTestFixtures by parser.option(
            ArgType.Boolean,
            shortName = "g",
            fullName = "generate-to-test-fixtures",
            description = "Assuming that generation is run at standard project root output path will be copied from input path except for source set testFixtures. For isntance if input file is src/main/com/my/company/example/Example.kt then output would be src/testFixtures/com/my/company/example/ExampleBuilder.kt. Setting overrides output file if such set."
        ).default(false)


        val lineEndingStyle by parser.option(
            ArgType.Choice<LineEnding>(),
            shortName = "l",
            fullName = "line-ending",
            description = "Line ending style to use for generated code"
        ).default(LineEnding.systemDefault())

        parser.parse(args)

        val generationOutputFile = when {
            generateToTestFixtures -> determineTestFixturesPath(sourceFile)
            else -> outputFile
        }

        println("Output file set to $generationOutputFile")

        return GenerationOptions(
            source = AstSource.File(sourceFile),
            output = FileOutput(generationOutputFile),
            lineEnding = lineEndingStyle
        )
    }

    private fun determineTestFixturesPath(sourceFile: String): String {
        val sourcePath = Path(sourceFile)
        val sourceRootDirIndex = sourcePath.indexOfFirst { it.name == "src" }
        if (sourcePath.count { it.name == "src" } != 1) {
            println("When generating test fixtures path must contain exactly one 'src' directory under which source sets lie.")
            systemExitManager.exit(1)
        }
        val commonPrefix = sourcePath.subpath(0, sourceRootDirIndex + 1)
        val fileNameIndex = sourcePath.nameCount - 1
        // Skip original source set name
        val commonSuffixDirPath = sourcePath.subpath(sourceRootDirIndex + 2, fileNameIndex)
        val inputFileName = sourcePath.getName(fileNameIndex).toString().substringBefore('.')
        return java.nio.file.Path
            .of(
                commonPrefix.toString(),
                "testFixtures",
                commonSuffixDirPath.toString(),
                "${inputFileName}Builder.kt"
            )
            .toString()
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val builderGenerationController = BuilderGenerationControllerImpl()
            BuilderGeneratorCli(builderGenerationController).run(args)
        }
    }
}
