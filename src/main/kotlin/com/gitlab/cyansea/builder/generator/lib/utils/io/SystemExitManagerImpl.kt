package com.gitlab.cyansea.builder.generator.lib.utils.io

import kotlin.system.exitProcess

class SystemExitManagerImpl : SystemExitManager {
    override fun exit(status: Int) {
        exitProcess(status)
    }
}
