package com.gitlab.cyansea.builder.generator.lib

data class Configuration(
    val lineSeparator: String = System.lineSeparator()
)
