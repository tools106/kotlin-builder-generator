package com.gitlab.cyansea.builder.generator.lib.internal

import com.gitlab.cyansea.builder.generator.lib.BuilderGenerator
import com.gitlab.cyansea.builder.generator.lib.Configuration
import com.gitlab.cyansea.builder.generator.lib.internal.types.TypeDefinition
import com.gitlab.cyansea.builder.generator.lib.model.Property
import com.gitlab.cyansea.builder.generator.lib.model.SourceClass
import com.gitlab.cyansea.builder.generator.lib.model.SourceFile
import com.gitlab.cyansea.builder.generator.lib.model.Type
import java.util.Locale

class BuilderGeneratorImpl(
    configuration: Configuration
) : BuilderGenerator {
    private val stringAppender = StringAppender(lineSeparator = configuration.lineSeparator)
    private val recognizedBuiltInTypes = listOf(
        TypeDefinition(
            identifier = "Int",
            defaultValue = "0"
        ),
        TypeDefinition(
            identifier = "Long",
            defaultValue = "0"
        ),
        TypeDefinition(
            identifier = "Boolean",
            defaultValue = "false"
        ),
        TypeDefinition(
            identifier = "String",
            defaultValue = "\"\""
        ),
        TypeDefinition(
            identifier = "LocalDate",
            defaultValue = "LocalDate.MIN"
        ),
        TypeDefinition(
            identifier = "OffsetDateTime",
            defaultValue = "OffsetDateTime.MIN"
        ),
        TypeDefinition(
            identifier = "Collection",
            isCollection = true,
            defaultValue = "mutableListOf()",
            generatedTypeOverride = "MutableCollection"
        ),
        TypeDefinition(
            identifier = "MutableCollection",
            isCollection = true,
            defaultValue = "mutableListOf()"
        ),
        TypeDefinition(
            identifier = "List",
            isCollection = true,
            defaultValue = "mutableListOf()",
            generatedTypeOverride = "MutableList"
        ),
        TypeDefinition(
            identifier = "MutableList",
            isCollection = true,
            defaultValue = "mutableListOf()"
        ),
        TypeDefinition(
            identifier = "Set",
            isCollection = true,
            defaultValue = "mutableSetOf()",
            generatedTypeOverride = "MutableSet"
        ),
        TypeDefinition(
            identifier = "MutableSet",
            isCollection = true,
            defaultValue = "mutableSetOf()"
        ),
        TypeDefinition(
            identifier = "Map",
            isMap = true,
            defaultValue = "mutableMapOf()",
            generatedTypeOverride = "MutableMap"
        ),
        TypeDefinition(
            identifier = "MutableMap",
            isMap = true,
            defaultValue = "mutableMapOf()"
        ),
    ).associateBy(TypeDefinition::identifier)

    override fun generate(sourceFile: SourceFile): String {
        sourceFile.validate()
        val packageHeader = sourceFile.packageDeclaration

        if (packageHeader != null) {
            stringAppender.appendLine("package $packageHeader").appendLine()
        }
        generateImports(sourceFile)

        sourceFile.classes.forEachIndexed { i, clazz ->
            generateClassBuilder(clazz)
            if (i != sourceFile.classes.indices.last) {
                stringAppender.appendLine()
            }
        }

        return stringAppender.toString()
    }

    private fun generateImports(sourceFile: SourceFile) {
        // Recognize imports
        val importedClasses = sourceFile.imports.associateBy { it.substringAfterLast(".") }
        val recognizedImports = importedClasses.filter { (importedClassName, _) ->
            sourceFile.classes.any { klass ->
                klass.properties.any { it.type.identifier == importedClassName }
            }
        }

        recognizedImports.forEach { (_, fullyQualifiedClass) ->
            stringAppender.appendLine("import $fullyQualifiedClass")
        }

        if (recognizedImports.isNotEmpty()) {
            stringAppender.appendLine()
        }
    }

    private fun generateClassBuilder(sourceClass: SourceClass) {
        generateClassSignature(sourceClass)
        stringAppender.increaseIndent()
        generatePropertyDeclarations(sourceClass)

        stringAppender.appendLine()
        generatePropertySetters(sourceClass)

        stringAppender.appendLine()
        generateBuildCommand(sourceClass)

        stringAppender.appendLine()
        generateCompanionShortcuts(sourceClass)

        stringAppender.decreaseIndent()
        generateClassEnd()
    }

    private fun generateCompanionShortcuts(sourceClass: SourceClass) {
        val className = sourceClass.name
        val builderClassName = "${className}Builder"

        stringAppender.appendLine("companion object {")
            .increaseIndent()
            .appendLine("fun build() : $className = $builderClassName().build()")
            .appendLine()
            .appendLine("fun build(builderSequence: $builderClassName.()->$builderClassName) : $className = $builderClassName().builderSequence().build()")
            .decreaseIndent()
            .appendLine("}")
    }

    private fun generateBuildCommand(sourceClass: SourceClass) {
        val className = sourceClass.name
        stringAppender
            .appendLine("fun build() : $className = $className(")
            .increaseIndent()

        sourceClass.properties.forEach { property ->
            val name = property.name
            stringAppender.appendLine("$name = $name,")
        }

        stringAppender.decreaseIndent().appendLine(")")
    }

    private fun generatePropertySetters(sourceClass: SourceClass) {
        sourceClass.properties.forEachIndexed { index, property ->
            val propertyType = property.type
            val correspondingBuiltInType = recognizedBuiltInTypes[propertyType.identifier]

            generateBasicSetter(property)

            if (correspondingBuiltInType?.isCollection == true && !propertyType.isNullable) {
                generateCollectionEntryAdder(property)
                if (!isBuiltInType(propertyType.parameters.first().identifier)) {
                    generateCollectionEntryAdderWithBuilder(property)
                }
            }

            if (correspondingBuiltInType?.isMap == true && !propertyType.isNullable) {
                generateMapSingleEntrySetter(property)
                if (!isBuiltInType(propertyType.parameters[1].identifier)) {
                    generateMapEntrySetterWithBuilderForValue(property)
                }
            }

            if (property.isUnrecognizedType()) {
                generateSetterByBuilderDelegation(property)
            }

            if (index != sourceClass.properties.indices.last) {
                stringAppender.appendLine()
            }
        }
    }

    private fun generateMapSingleEntrySetter(property: Property) {
        val name = property.name
        val keyType = property.rawType.substringBefore(',').substringAfter('<').trim()
        val valueType = property.rawType.substringAfter(',').substringBeforeLast('>').trim()
        val entrySetterName = "set${name.toCapitalized()}Entry"

        stringAppender.appendLine("fun $entrySetterName(key : $keyType, value : $valueType) = apply { this.$name[key] = value }")
    }

    private fun generateMapEntrySetterWithBuilderForValue(property: Property) {
        val name = property.name
        val keyType = property.rawType.substringBefore(',').substringAfter('<').trim()
        val valueBuilderType = "${property.type.parameters[1].identifier}Builder"
        val entrySetterName = "set${name.toCapitalized()}Entry"

        stringAppender.appendLine(
            "fun $entrySetterName(key : $keyType, builderSequence : $valueBuilderType.()->$valueBuilderType) =" +
                    " $entrySetterName(key, $valueBuilderType.build(builderSequence))"
        )
    }

    // Note that this only works for MutableCollection inheritors.
    private fun generateCollectionEntryAdder(property: Property) {
        val name = property.name
        val typeIdentifier = property.type.identifier
        val contentType = property.rawType.removeSurrounding("${typeIdentifier}<", ">")
        val entryAdderName = "add${name.toCapitalized()}"

        stringAppender.appendLine("fun $entryAdderName($name : $contentType) = apply { this.$name.add($name) }")
    }

    // Depends on single entry adder being generated beforehand
    private fun generateCollectionEntryAdderWithBuilder(property: Property) {
        val name = property.name
        val adderName = "add${name.toCapitalized()}"
        val contentBuilderType = "${property.type.parameters.first().identifier}Builder"

        stringAppender.appendLine(
            "fun $adderName(builderSequence : $contentBuilderType.()->$contentBuilderType) =" +
                    " $adderName($contentBuilderType.build(builderSequence))"
        )
    }

    private fun String.toCapitalized() =
        replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.getDefault()) else it.toString() }

    private fun Property.isUnrecognizedType(): Boolean = !isRecognizedBuiltInType()
    private fun Property.isRecognizedBuiltInType(): Boolean = isBuiltInType(type.identifier)
    private fun isBuiltInType(typeIdentifier: String): Boolean = recognizedBuiltInTypes.containsKey(typeIdentifier)

    private fun generateSetterByBuilderDelegation(property: Property) {
        val name = property.name
        val propertyBuilderClass = "${property.type.identifier}Builder"

        stringAppender.appendLine(
            "fun $name(builderSequence : $propertyBuilderClass.()->$propertyBuilderClass) = " +
                    "$name($propertyBuilderClass.build(builderSequence))"
        )
    }

    private fun generateBasicSetter(property: Property) {
        val name = property.name
        val type = determineVariableType(property)

        stringAppender.appendLine("fun $name($name : $type) = apply { this.$name = $name }")
    }

    private fun generatePropertyDeclarations(sourceClass: SourceClass) {
        sourceClass.properties.forEach { property ->
            val name = property.name
            val type = determineVariableType(property)

            val defaultValue = determineDefaultValue(property.type)

            stringAppender.appendLine("private var $name: $type = $defaultValue")
        }
    }

    private fun determineVariableType(property: Property): String {
        val correspondingBuiltInType = recognizedBuiltInTypes[property.type.identifier]
        return if (correspondingBuiltInType?.generatedTypeOverride != null) {
            property.rawType.replaceFirst(
                correspondingBuiltInType.identifier,
                correspondingBuiltInType.generatedTypeOverride
            )
        } else {
            property.rawType
        }
    }

    private fun determineDefaultValue(type: Type): String = when {
        type.isNullable -> "null"
        isBuiltInType(type.identifier) -> recognizedBuiltInTypes.getValue(type.identifier).defaultValue
        else -> "${type.identifier}Builder.build()"
    }

    private fun generateClassSignature(sourceClass: SourceClass) {
        stringAppender
            .appendLine("/**")
            .appendLine("* The following class has been generated by com.gitlab.cyansea.builder.generator.")
            .appendLine("*/")
            .appendLine("class ${sourceClass.name}Builder {")
    }

    private fun generateClassEnd() {
        stringAppender.appendLine("}")
    }
}
