package com.gitlab.cyansea.builder.generator.lib.utils.io

interface Output {
    fun writeText(text: String)
}
