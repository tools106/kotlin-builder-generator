package com.gitlab.cyansea.builder.generator.lib.utils.io

import java.io.File

class FileOutput(
    fileName: String
) : Output {
    private val file = File(fileName)

    override fun writeText(text: String) {
        file.writeText(text)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as FileOutput

        if (file != other.file) return false

        return true
    }

    override fun hashCode(): Int {
        return file.hashCode()
    }

    override fun toString(): String {
        return "FileOutput(file=$file)"
    }
}
