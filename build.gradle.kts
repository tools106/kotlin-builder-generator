import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.w3c.dom.Node

val javaVersion: JavaVersion by extra(JavaVersion.VERSION_11)

if (JavaVersion.current() != javaVersion) {
    throw GradleException("Java $javaVersion required!")
}

plugins {
    kotlin("jvm") version "1.6.10"
    application
    jacoco
    id("java-test-fixtures")
}

application {
    mainClass.set("com.gitlab.cyansea.builder.generator.BuilderGeneratorCli")
}

tasks.withType<KotlinCompile> {
    kotlinOptions {
        allWarningsAsErrors = true
        suppressWarnings = false

        freeCompilerArgs = listOf("-Xjsr305=strict")

        jvmTarget = javaVersion.toString()
        sourceCompatibility = javaVersion.toString()
        targetCompatibility = javaVersion.toString()
    }
}

tasks.test {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)
}

jacoco {
    toolVersion = "0.8.7"
    reportsDirectory.set(file("$buildDir/reports/jacoco"))
}

tasks.jacocoTestReport {
    reports {
        csv.required.set(false)
        xml.required.set(true)
        html.required.set(true)
    }
}

fun Node.findChildren(tag: String): List<Node> {
    val children = mutableListOf<Node>()

    for (i in 0 until childNodes.length) {
        if (childNodes.item(i).nodeName == tag) {
            children.add(childNodes.item(i))
        }
    }

    return children
}

fun printCoverage(node: Node) {
    val type = node.attributes.getNamedItem("type").nodeValue
    val missed = node.attributes.getNamedItem("missed").nodeValue.toInt()
    val covered = node.attributes.getNamedItem("covered").nodeValue.toInt()

    val coverage = if (missed == 0) 100 else 100 * covered / (missed + covered)
    println("${"%11s".format(type)}: ${coverage}% covered")
}

tasks.register("printJacocoNumbers") {
    group = "other"
    description = "Print simple numbers from aggregate coverage."

    dependsOn(tasks.jacocoTestReport)

    doLast {
        val factory = javax.xml.parsers.DocumentBuilderFactory.newInstance()
        factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false)
        factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false)

        val builder = factory.newDocumentBuilder()
        val document = builder.parse(tasks.jacocoTestReport.get().reports.xml.outputLocation.asFile.get())

        val counters = document.getElementsByTagName("report").item(0)!!.findChildren("counter")


        println("==============================================")
        println("                 CODE COVERAGE                ")
        counters.forEach(::printCoverage)
        println("==============================================")


    }
}

repositories {
    mavenCentral()
    maven("https://jitpack.io") {
        content {
            includeGroup("com.github.kotlinx")
            includeGroup("com.github.kotlinx.ast")
            includeGroup("com.github.drieks.antlr-kotlin")
        }
    }
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-cli:0.3.4")
    implementation("com.github.kotlinx.ast:grammar-kotlin-parser-antlr-kotlin:0.1.0")

    testFixturesApi("com.github.kotlinx.ast:grammar-kotlin-parser-antlr-kotlin:0.1.0")

    testImplementation("org.junit.jupiter:junit-jupiter:5.8.2")
    testImplementation("org.assertj:assertj-core:3.22.0")
    testImplementation("org.mockito.kotlin:mockito-kotlin:4.0.0")

}
